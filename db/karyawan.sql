-- --------------------------------------------------------
-- Host:                         127.0.0.1
-- Server version:               10.4.14-MariaDB - mariadb.org binary distribution
-- Server OS:                    Win64
-- HeidiSQL Version:             11.0.0.5919
-- --------------------------------------------------------

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET NAMES utf8 */;
/*!50503 SET NAMES utf8mb4 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;


-- Dumping database structure for karyawan
CREATE DATABASE IF NOT EXISTS `karyawan` /*!40100 DEFAULT CHARACTER SET utf8mb4 */;
USE `karyawan`;

-- Dumping structure for table karyawan.detail_karyawan
CREATE TABLE IF NOT EXISTS `detail_karyawan` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `karyawan_id` int(3) NOT NULL,
  `gapok` int(7) NOT NULL,
  `status` varchar(30) NOT NULL,
  `jenis_kelamin` varchar(30) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table karyawan.detail_karyawan: ~4 rows (approximately)
/*!40000 ALTER TABLE `detail_karyawan` DISABLE KEYS */;
INSERT INTO `detail_karyawan` (`id`, `karyawan_id`, `gapok`, `status`, `jenis_kelamin`, `created_date`) VALUES
	(1, 1, 2000000, 'lajang', 'laki laki', '2020-09-10 21:04:25'),
	(2, 2, 3000000, 'menikah', 'laki laki', '2020-09-10 21:04:25'),
	(3, 3, 2000000, 'lajang', 'menikah', '2020-09-10 21:05:12'),
	(4, 4, 2000000, 'menikah', 'perempuan', '2020-09-10 21:05:12');
/*!40000 ALTER TABLE `detail_karyawan` ENABLE KEYS */;

-- Dumping structure for table karyawan.karyawan
CREATE TABLE IF NOT EXISTS `karyawan` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `nama` varchar(50) NOT NULL,
  `created_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;

-- Dumping data for table karyawan.karyawan: ~4 rows (approximately)
/*!40000 ALTER TABLE `karyawan` DISABLE KEYS */;
INSERT INTO `karyawan` (`id`, `nama`, `created_date`) VALUES
	(1, 'Andi', '2020-09-10 21:01:07'),
	(2, 'Budi', '2020-09-10 21:01:07'),
	(3, 'Candra', '2020-09-10 21:01:22'),
	(4, 'Suci', '2020-09-10 21:01:22');
/*!40000 ALTER TABLE `karyawan` ENABLE KEYS */;

-- Dumping structure for table karyawan.prestasi
CREATE TABLE IF NOT EXISTS `prestasi` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `karyawan_id` int(11) NOT NULL,
  `prestasi` varchar(100) NOT NULL,
  `tahun` int(4) NOT NULL,
  `crated_date` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;

-- Dumping data for table karyawan.prestasi: ~6 rows (approximately)
/*!40000 ALTER TABLE `prestasi` DISABLE KEYS */;
INSERT INTO `prestasi` (`id`, `karyawan_id`, `prestasi`, `tahun`, `crated_date`) VALUES
	(1, 1, 'Karyawan Teladan', 2010, '2020-09-10 21:11:09'),
	(2, 2, 'Paduan suara tingkat nasional', 2011, '2020-09-10 21:11:09'),
	(3, 1, 'Lomba Web Developer Tingkat Prov Jakarta', 2012, '2020-09-10 21:11:40'),
	(4, 2, 'Lomba menyanyi indonesian idol', 2016, '2020-09-10 21:11:40'),
	(5, 3, 'Lomba lari marathon tingkat kecamatan', 2015, '2020-09-10 21:12:27'),
	(6, 3, 'Lomba lari marathon tingkat provinsi', 2016, '2020-09-10 21:12:35');
/*!40000 ALTER TABLE `prestasi` ENABLE KEYS */;

/*!40101 SET SQL_MODE=IFNULL(@OLD_SQL_MODE, '') */;
/*!40014 SET FOREIGN_KEY_CHECKS=IF(@OLD_FOREIGN_KEY_CHECKS IS NULL, 1, @OLD_FOREIGN_KEY_CHECKS) */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
