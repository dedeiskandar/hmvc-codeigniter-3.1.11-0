<?php
class M_karyawan extends CI_Model{

  function tampil_data(){
    $query="SELECT a.nama,
                   b.gapok,
                   b.status,
            		   b.jenis_kelamin,
                   a.id
             FROM karyawan a
             LEFT JOIN detail_karyawan b ON a.id = b.karyawan_id";
      $sql=$this->db->query($query);
      if($sql->num_rows() > 0){
              return $sql->result();
        }else{
           return 0;
        }
	}

  function lihat_prestasi($id){
    $query="SELECT a.nama,
                   c.prestasi,
        		       c.tahun
            FROM karyawan a
              LEFT JOIN detail_karyawan b ON a.id = b.karyawan_id
              LEFT JOIN prestasi c ON a.id = c.karyawan_id  WHERE a.id=$id  ";
      $sql=$this->db->query($query);
      if($sql->num_rows() > 0){
              return $sql->result();
        }else{
           return 0;
        }

  }
}
